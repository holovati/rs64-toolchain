#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
COREC="$(cat /proc/cpuinfo | awk '/^processor/{print $3}' | wc -l)"
#COREC=1

CWD="$(echo $PWD)"



mkdir -pv $CWD/build/musl
mkdir -pv $CWD/build/binutils
mkdir -pv $CWD/build/gcc
mkdir -pv $CWD/sysroot

mkdir -pv $CWD/tmp_sysroot

mkdir -pv $CWD/sysroot/usr/lib
mkdir -pv $CWD/sysroot/usr/bin

set -e


######### BINUTILS PASS 1 ########
pushd $CWD/build/binutils
$DIR/binutils-2.31.1/configure \
    --target=x86_64-rs64-musl \
    --prefix="$CWD/tmp_sysroot/usr" \
    --with-sysroot=$CWD/tmp_sysroot \
    --disable-werror \
    --disable-nls

make -j$COREC
make install

rm -rf *
popd

######### RS64 GCC PASS 1 ########
pushd $CWD/build/gcc

pushd $DIR/gcc-8.2.0
contrib/download_prerequisites
popd

$DIR/gcc-8.2.0/configure \
    --target=x86_64-rs64-musl \
    --prefix="$CWD/tmp_sysroot/usr" \
    --with-sysroot=$CWD/tmp_sysroot \
    --with-newlib \
    --enable-languages=c,c++ \
    --disable-multilib \
    --disable-shared \
    --without-headers \
    --disable-nls \
    --disable-decimal-float \
    --disable-threads \
    --disable-libatomic \
    --disable-libgomp \
    --disable-libquadmath \
    --disable-libssp \
    --disable-libvtv \
    --disable-libstdcxx

make all-gcc all-target-libgcc -j$COREC
make install-gcc install-target-libgcc

rm -rf *

popd

######### MUSL ########
pushd $CWD/build/musl

$DIR/musl/configure \
    --prefix="$CWD/sysroot/usr" \
    --exec-prefix="$CWD/sysroot/usr" \
    CROSS_COMPILE="$CWD/tmp_sysroot/usr/bin/x86_64-rs64-musl-" \
    CFLAGS="-DSYS_fork"

make -j$COREC
make install

popd

######### BINUTILS PASS 2 ########
pushd $CWD/build/binutils
$DIR/binutils-2.31.1/configure \
    --target=x86_64-rs64-musl \
    --prefix="$CWD/sysroot/usr" \
    --with-sysroot=$CWD/sysroot \
    --disable-werror \
    --enable-shared \
    --disable-nls

make -j$COREC
make install

popd

######### RS64 GCC PASS 2 ########
pushd $CWD/build/gcc

$DIR/gcc-8.2.0/configure \
    --target=x86_64-rs64-musl \
    --prefix="$CWD/sysroot/usr" \
    --with-sysroot=$CWD/sysroot \
    --enable-languages=c,c++ \
    --disable-multilib \
    --enable-shared \
    --enable-initfini-array \
    --disable-nls \
    --disable-decimal-float \
    --disable-libatomic \
    --disable-libgomp \
    --disable-libquadmath \
    --disable-libssp \
    --disable-libvtv \
    --with-gxx-include-dir="$CWD/sysroot/usr/include/c++/8.2.0" \
    --disable-libstdcxx-pch

make -j$COREC
make install

popd

pushd $CWD/build
git clone https://gitlab.com/holovati/rs64-gcc-plugin.git
pushd rs64-gcc-plugin
mkdir -v build
pushd build
export PATH=$CWD/sysroot/usr/bin:$PATH
cmake ../.
make
make install
popd
popd
popd


rm -rf $CWD/build
rm -rf $CWD/tmp_sysroot

#clear

printf "$CWD/sysroot contains the new rs64 toolchain\n"
