
MUSL PASS 1 CMDS:

```../../rs64-toolchain/musl/configure --prefix="$HOME/rs64-sysroot/usr" --exec-prefix="%HOME/rs64-sysroot/usr" --enable-wrapper=false ```

```make```

```make install```

```rm -rf %HOME/rs64-sysroot/usr/lib```

BINUTILS:

``` ../../rs64-toolchain/binutils-2.31.1/configure --target=x86_64-rs64 --prefix="$HOME/rs64-sysroot/usr" --with-sysroot="$HOME/rs64-sysroot" --disable-werror ```

```make```

```make install```

GCC:

``` ../../rs64-toolchain/gcc-8.2.0/configure --target=x86_64-rs64 --prefix="$HOME/rs64-sysroot/usr" --with-sysroot=$HOME/rs64-sysroot --enable-languages=c,c++ ```

``` make all-gcc all-target-libgcc ```
``` make install-gcc install-target-libgcc ```

MUSL PASS 2 CMDS:
