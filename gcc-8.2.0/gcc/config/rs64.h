#undef GNU_USER_TARGET_OS_CPP_BUILTINS
#define GNU_USER_TARGET_OS_CPP_BUILTINS()			\
    do {							\
	if (OPTION_GLIBC)					\
	  builtin_define ("__gnu_rs64__");			\
	builtin_define_std ("rs64");				\
	builtin_define_std ("unix");				\
	builtin_assert ("system=rs64");				\
	builtin_assert ("system=unix");				\
	builtin_assert ("system=posix");			\
    } while (0)

#undef ANDROID_TARGET_OS_CPP_BUILTINS
#define ANDROID_TARGET_OS_CPP_BUILTINS() /**/

#define NOANDROID "!mandroid"

#define LINUX_OR_ANDROID_CC(LINUX_SPEC, ANDROID_SPEC) \
  "%{" NOANDROID "|tno-android-cc:" LINUX_SPEC ";:" ANDROID_SPEC "}"

#define LINUX_OR_ANDROID_LD(LINUX_SPEC, ANDROID_SPEC) \
  "%{" NOANDROID "|tno-android-ld:" LINUX_SPEC ";:" ANDROID_SPEC "}"


#undef ANDROID_LINK_SPEC 
#define ANDROID_LINK_SPEC 

#undef ANDROID_CC1_SPEC
#define ANDROID_CC1_SPEC

#undef ANDROID_CC1PLUS_SPEC
#define ANDROID_CC1PLUS_SPEC

#undef ANDROID_LIB_SPEC
#define ANDROID_LIB_SPEC

#undef ANDROID_STARTFILE_SPEC
#define ANDROID_STARTFILE_SPEC

#undef ANDROID_ENDFILE_SPEC
#define ANDROID_ENDFILE_SPEC
